<?php include 'template/header.php';?>
<div class="ft-content bg-white-100">
  <div class="ft-block">
    <h1 class="ft-heading1">Getting Started</h1>
    <p>Get started with FDA UI Kit with instruction belows.</p>
    <h2 class="ft-h3">Copy & Paste to your header !</h2>
    <div class="ft-code-example">
      <pre class="code" lang="html">
  &lt;link rel=&quot;stylesheet&quot; href=&quot;<?=WEB_META_BASE_URL?>css/ft_style.css&quot;&gt;
  &lt;script type=&quot;text/javascript&quot; src=&quot;<?=WEB_META_BASE_URL?>js/ft_script.js&quot;&gt;&lt;/script&gt;</pre>
    </div>
    <h2 class="ft-h3" style="margin-top: 1.25rem;">HTML Starter</h2>
    <p>Download <a class="text-danger-80" href="<?=WEB_META_BASE_URL?>download.php">StarterSite.zip</a></p>
    <p><i>Last Updates : 5 Oct 2021</i></p>
    <ul>
      <li>
        8 Oct 2021 -- Mobile Tab Layout
      </li>
      <li>
        5 Oct 2021 -- Responsive Layout
      </li>
      <li>
        30 Sep 2021 -- Menu Layout
      </li>
    </ul>
  </div>
</div>
<?php include 'template/footer.php'; ?>