<?php include 'template/header.php';?>
<div class="ft-content">
    <div class="ft-block">
        <div class="ft-h1 ft-title">
            Mobile Tab Layout
        </div>
        <p style="margin-bottom: 50px;">This layout will show tab button on screen below <b class="ft-text-danger-80">768 px</b></p>
        <div style="margin-bottom: 20px;" class="ft-h4"><center>Mobile Layout</center></div>
        <div style="background-color: #f5f5f5; max-width: 320px; margin-left: auto; margin-right: auto; border: 2px solid #296838; border-radius: 4px; padding-top: 30px; padding-bottom: 30px; margin-bottom: 40px;"
            class="ft-container ft-3-part ft-tab-layout show-example">
            <div class="ft-head-tab">
                <div class="ft-tab-row">
                    <div data-tab="left" class="ft-tab-btn left ft-h6"><i style="margin-right: 8px;" class="fas fa-th-large"></i> ขั้นตอนคำขอ</div>
                    <div data-tab="right" class="ft-tab-btn right ft-h6"><i style="margin-right: 8px;" class="fas fa-history"></i> ประวัติรายการ</div>
                </div>
                <div class="ft-tab-row">
                    <div data-tab="main" class="ft-tab-btn main ft-h4">
                        รายละเอียดคำขอ
                    </div>
                </div>
            </div>
            <div class="ft-body-tab">
                <div class="ft-aside-l tab-item left">
                    <div class="ft-content">
                        <div class="ft-block">
                            <p class="ft-h5">ขั้นตอนคำขอ</p>
                            <ol class="ft-h6">
                                <li>สร้างคำขอ</li>
                                <li>รายละเอียดคำขอ</li>
                                <li>ยืนยันคำขอ</li>
                                <li>สั่งชำระคำขอ</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="ft-main tab-item main">
                    <div class="ft-content">
                        <div class="ft-block">
                            <p class="ft-h5">รายละเอียดคำขอ</p>
                            <p>Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
                        </div>
                    </div>
                </div>
                <div class="ft-aside-r tab-item right">
                    <div class="ft-content">
                        <div class="ft-block">
                            <p class="ft-h5">ประวัติรายการ</p>
                            <div class="ft-text-grey-100" style="border-bottom: 1px solid #646d82; padding: 20px 25px 10px;">
                                <div class="ft-h6">นาย เอก ขยันทำงาน</div>
                                <div class="ft-h6" style="margin-bottom: 25px;">11/11/21 : 12.00</div>
                                <h4><b>แก้ไข</b></h4>
                            </div>
                            <div class="ft-text-grey-100" style="border-bottom: 1px solid #646d82; padding: 20px 25px 10px;">
                                <div class="ft-h6">นาย เอก ขยันทำงาน</div>
                                <div class="ft-h6" style="margin-bottom: 25px;">11/11/21 : 12.00</div>
                                <h4><b>ขอเอกสารเพิ่ม</b></h4>
                            </div>
                            <div class="ft-text-grey-100" style="border-bottom: 1px solid #646d82; padding: 20px 25px 10px;">
                                <div class="ft-h6">นาย เอก ขยันทำงาน</div>
                                <div class="ft-h6" style="margin-bottom: 25px;">11/11/21 : 12.00</div>
                                <h4><b>อัพโหลดเอกสาร</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 20px;" class="ft-h4"><center>Desktop Layout</center></div>
        <div style="background-color: #f5f5f5; border: 2px solid #296838; border-radius: 4px; padding-top: 30px; padding-bottom: 30px; margin-bottom: 40px;" class="ft-container ft-3-part ft-tab-layout">
            <div class="ft-head-tab">
                <div class="ft-tab-row">
                    <div data-tab="left" class="ft-tab-btn left ft-h6"><i style="margin-right: 8px;" class="fas fa-th-large"></i> ขั้นตอนคำขอ</div>
                    <div data-tab="right" class="ft-tab-btn right ft-h6"><i style="margin-right: 8px;" class="fas fa-history"></i> ประวัติรายการ</div>
                </div>
                <div class="ft-tab-row">
                    <div data-tab="main" class="ft-tab-btn main ft-h4">
                        รายละเอียดคำขอ
                    </div>
                </div>
            </div>
            <div class="ft-body-tab">
                <div class="ft-aside-l tab-item left">
                    <div class="ft-content">
                        <div class="ft-block">
                            <p class="ft-h5">ขั้นตอนคำขอ</p>
                            <ol class="ft-h6">
                                <li>สร้างคำขอ</li>
                                <li>รายละเอียดคำขอ</li>
                                <li>ยืนยันคำขอ</li>
                                <li>สั่งชำระคำขอ</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="ft-main tab-item main">
                    <div class="ft-content">
                        <div class="ft-block">
                            <p class="ft-h5">รายละเอียดคำขอ</p>
                            <p>Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
                        </div>
                    </div>
                </div>
                <div class="ft-aside-r tab-item right">
                    <div class="ft-content">
                        <div class="ft-block">
                            <p class="ft-h5">ประวัติรายการ</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing, elit. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ft-code-example">
           <pre class="code" lang="html">
<div style="background-color: #f5f5f5; border: 2px solid #296838; border-radius: 4px; padding-top: 30px; padding-bottom: 30px; margin-bottom: 40px;" class="ft-container ft-3-part ft-tab-layout">
    <div class="ft-head-tab">
        <div class="ft-tab-row">
            <div data-tab="left" class="ft-tab-btn left ft-h6"><i style="margin-right: 8px;" class="fas fa-th-large"></i> ขั้นตอนคำขอ</div>
            <div data-tab="right" class="ft-tab-btn right ft-h6"><i style="margin-right: 8px;" class="fas fa-history"></i> ประวัติรายการ</div>
        </div>
        <div class="ft-tab-row">
            <div data-tab="main" class="ft-tab-btn main ft-h4">
                รายละเอียดคำขอ
            </div>
        </div>
    </div>
    <div class="ft-body-tab">
        <div class="ft-aside-l tab-item left">
            <div class="ft-content">
                <div class="ft-block">
                    <p class="ft-h5">ขั้นตอนคำขอ</p>
                    <ol class="ft-h6">
                        <li>สร้างคำขอ</li>
                        <li>รายละเอียดคำขอ</li>
                        <li>ยืนยันคำขอ</li>
                        <li>สั่งชำระคำขอ</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="ft-main tab-item main">
            <div class="ft-content">
                <div class="ft-block">
                    <p class="ft-h5">รายละเอียดคำขอ</p>
                    <p>Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
                </div>
            </div>
        </div>
        <div class="ft-aside-r tab-item right">
            <div class="ft-content">
                <div class="ft-block">
                    <p class="ft-h5">ประวัติรายการ</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing, elit. </p>
                </div>
            </div>
        </div>
    </div>
</div></pre></div>
    </div>
</div>
<?php include 'template/footer.php'; ?>