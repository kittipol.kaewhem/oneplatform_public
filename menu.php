<?php include 'template/header.php';?>
<div class="ft-content bg-white-100">
  <div class="ft-block ">
    <h1 class="ft-title">
      Menu Header
    </h1>
    <h2>Column Menu</h2>
    <div class="main-header show-case ">
      <div class="mh-group menu-group">
        <div class="mg-item mg-alert always-on">
          <div class="mgi-sign system"></div>
          <div class="mgi-text">ระบบงานอื่น</div>
          <div class="menu-list">
            <div class="wrap-menu-list">
              <?php for ($i=0; $i < 30; $i++) { ?>
              <div class="ml-item">
                <p><img src="<?=WEB_META_BASE_URL?>/images/menu-list-example.svg" alt=""></p>
                <p class="ft-text-green-80">ระบบ <?=$i+1?></p>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="mg-item">
        </div>
      </div>
    </div>
    <div style="margin-bottom: 40px;" class="ft-code-example">
      <pre class="code" lang="html">
<div class="main-header">
  <div class="mh-group menu-group">
    <div class="mg-item mg-alert always-on">
      <div class="mgi-sign system"></div>
      <div class="mgi-text">ระบบงานอื่น</div>
      <div class="menu-list">
        <div class="wrap-menu-list">
          <div class="ml-item">
            <p><img src="http://localhost/oneplatform_public//images/menu-list-example.svg" alt=""></p>
            <p class="ft-text-green-80">ระบบ 1</p>
          </div>
          <div class="ml-item">
            <p><img src="http://localhost/oneplatform_public//images/menu-list-example.svg" alt=""></p>
            <p class="ft-text-green-80">ระบบ 2</p>
          </div>
          <div class="ml-item">
            <p><img src="http://localhost/oneplatform_public//images/menu-list-example.svg" alt=""></p>
            <p class="ft-text-green-80">ระบบ 3</p>
          </div>
        </div>
      </div>
    </div>
    <div class="mg-item"></div>
  </div>
</div></pre>
    </div>
    <h2>Stretch Menu</h2>
    <div class="main-header show-case">
      <div class="mh-group menu-group">
        <div class="mg-item mg-alert always-on">
          <div class="wrap-mgi">
            <div class="mgi-sign announce"></div>
            <div class="mgi-text">ประกาศ</div>
          </div>
          <div class="menu-list stretch">
            <div class="wrap-menu-list">
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/email.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    E-Mail แจ้งเตือน
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/msg.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    SMS แจ้งเตือน
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/alert.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    Alert. !!!
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/email.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    E-Mail แจ้งเตือน
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/msg.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    SMS แจ้งเตือน
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/alert.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    Alert. !!!
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>

              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/email.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    E-Mail แจ้งเตือน
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/msg.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    SMS แจ้งเตือน
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
              <div class="ml-item">
                <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
                  <img src="<?=WEB_META_BASE_URL?>/images/alert.svg" alt="">
                </span><span style="display: inline-block;vertical-align: middle;">
                  <span class="ft-text-green-80">
                    Alert. !!!
                  </span><br/>
                  <span class="ft-text-grey-8">
                    XXXXXXXXXX
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="mg-item">
        </div>
      </div>
    </div>
    <div class="ft-code-example">
      <pre class="code" lang="html">
<div class="main-header">
  <div class="mh-group menu-group">
    <div class="mg-item mg-alert">
      <div class="wrap-mgi">
        <div class="mgi-sign announce"></div>
        <div class="mgi-text">ประกาศ</div>
      </div>
      <div class="menu-list stretch">
        <div class="wrap-menu-list">
          <div class="ml-item">
            <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
              <img src="<?=WEB_META_BASE_URL?>/images/email.svg" alt="">
            </span><span style="display: inline-block;vertical-align: middle;">
              <span class="ft-text-green-80">
                E-Mail แจ้งเตือน
              </span><br/>
              <span class="ft-text-grey-8">
                XXXXXXXXXX
              </span>
            </span>
          </div>
          <div class="ml-item">
            <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
              <img src="<?=WEB_META_BASE_URL?>/images/msg.svg" alt="">
            </span><span style="display: inline-block;vertical-align: middle;">
              <span class="ft-text-green-80">
                SMS แจ้งเตือน
              </span><br/>
              <span class="ft-text-grey-8">
                XXXXXXXXXX
              </span>
            </span>
          </div>
          <div class="ml-item">
            <span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
              <img src="<?=WEB_META_BASE_URL?>/images/alert.svg" alt="">
            </span><span style="display: inline-block;vertical-align: middle;">
              <span class="ft-text-green-80">
                Alert. !!!
              </span><br/>
              <span class="ft-text-grey-8">
                XXXXXXXXXX
              </span>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="mg-item"></div>
  </div>
</div></pre>
    </div>
  </div>
</div>
<?php include 'template/footer.php'; ?>