			</div>
			<div class="ft-aside-r">
				<div class="ft-content">
				  <div class="ft-block">
				  	<p class="ft-h3">SAMPLE SPACE</p>
				  	<p class="ft-h4">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
				  	<p>Eligendi laudantium, ipsa modi perferendis vitae fugiat unde sed commodi totam ullam atque laborum adipisci voluptatem quia provident possimus accusamus autem nulla?</p>
				  </div>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-floating btn-lg ft-bg-green-80 ft-text-white-100 "
		        id="gototop"
		        >
		  <i class="fas fa-arrow-up"></i>
		</button>
	</div>
	<footer id="ft-footer">
		<div class="main-footer">
			<div class="footer-content left">
				<a class="fcl-item" target="_blank" href="https://www.fda.moph.go.th/Pages/HomeP_D2.aspx">สำนักงานคณะกรรมการอาหารและยา กระทรวงสาธารณะสุข</a>
			</div>
			<div class="footer-content right">
				<a href="#" class="fcr-item">Legal Issues</a>
				<a href="#" class="fcr-item">HELP</a>
				<a href="#" class="fcr-item">POLICY</a>
				<a href="#" class="fcr-item">TERMS AND CONDITIONNS</a>
			</div>
		</div>
	</footer>
</body>
</html>