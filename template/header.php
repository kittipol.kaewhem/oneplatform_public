<?php
	include 'template/config.php';
	$uri = str_replace(".php","",str_replace(WEB_SLUG, "", $_SERVER["REQUEST_URI"]));
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FDA UI Kit</title>
	<!-- HEADER Include here -->
	<link rel="stylesheet" href="<?=WEB_META_BASE_URL?>css/bootstrap.min.css">
	<script type="text/javascript" src="<?=WEB_META_BASE_URL?>js/popper.min.js"></script>
	<script type="text/javascript" src="<?=WEB_META_BASE_URL?>js/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="<?=WEB_META_BASE_URL?>js/bootstrap.min.js"></script>

	<!-- Code Syntax Highlight -->
	<link rel="stylesheet" href="<?=WEB_META_BASE_URL?>css/jquery.highlight.css">
	<script type="text/javascript" src="<?=WEB_META_BASE_URL?>js/jquery.highlight.js"></script>
	<!-- Code Syntax Highlight -->

	<link rel="stylesheet" href="<?=WEB_META_BASE_URL?>css/fontawesome_all.min.css">
	<script type="text/javascript" src="<?=WEB_META_BASE_URL?>js/fontawesome_all.min.js"></script>

	<link rel="stylesheet" href="<?=WEB_META_BASE_URL?>css/ft_style.css">
	<script type="text/javascript" src="<?=WEB_META_BASE_URL?>js/ft_script.js"></script>
	<!-- HEADER Include here -->
	<script>
		var base_url = '<?php echo WEB_META_BASE_URL ; ?>';
	</script>
</head>
<body>
	<header id="ft-header">
		<div class="main-header">
			<div class="header-static">
				<div class="mh-group logo-site">
					<a href="<?=WEB_META_BASE_URL?>"><span class="logo-moph"></span><span class="logo-fda"></span><span class="title-site">FDA UI Kit</span></a>
				</div>
				<div class="mh-group menu-group">
					<div class="mg-item mg-alert">
						<div class="wrap-mgi">
							<div class="mgi-sign announce"></div>
							<div class="mgi-text">ประกาศ</div>
						</div>
						<div class="menu-list stretch">
							<div class="wrap-menu-list">
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/email.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											E-Mail แจ้งเตือน
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/msg.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											SMS แจ้งเตือน
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/alert.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											Alert. !!!
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>

								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/email.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											E-Mail แจ้งเตือน
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/msg.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											SMS แจ้งเตือน
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/alert.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											Alert. !!!
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>

								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/email.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											E-Mail แจ้งเตือน
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/msg.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											SMS แจ้งเตือน
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
								<div class="ml-item">
									<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
										<img src="<?=WEB_META_BASE_URL?>images/alert.svg" alt="">
									</span><span style="display: inline-block;vertical-align: middle;">
										<span class="ft-text-green-80">
											Alert. !!!
										</span><br/>
										<span class="ft-text-grey-8">
											XXXXXXXXXX
										</span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="mg-item">
						<div class="mgi-sign list"></div>
						<div class="mgi-text">ใบสั่งชำระ</div>
					</div>
					<div class="mg-item mg-alert">
						<div class="mgi-sign system"></div>
						<div class="mgi-text">ระบบงานอื่น</div>
						<div class="menu-list">
							<div class="wrap-menu-list">
								<?php for ($i=0; $i < 30; $i++) { ?>
								<div class="ml-item">
									<p><img src="<?=WEB_META_BASE_URL?>images/menu-list-example.svg" alt=""></p>
									<p class="ft-text-green-80">ระบบ <?=$i+1?></p>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="mg-item">
						<div class="mgi-sign noti"></div>
						<div class="mgi-text">แจ้งเตือน</div>
					</div>
					<div class="mg-item">
						<div class="mgi-sign company"></div>
						<div class="mgi-text s-16">
							Products Profile<br>บ. ขายยาดี จำกัด
						</div>
					</div>
				</div>
				<div class="mh-group user-login">
					<div id="ft-menu-canvas" class="ft-menu-canvas"><i class="fas fa-bars ft-menu-canvas-open"></i><i class="fas fa-times ft-menu-canvas-close"></i></div>
					<div class="usl-name">นาย เอก ขยันทำงาน</div>
					<div class="usl-img"><img src="<?=WEB_META_BASE_URL?>images/ex_icon.png" alt=""></div>
				</div>
			</div>
			<div class="header-canvas">
				<div class="mh-group menu-group">
					<div class="mg-item mg-alert">
						<div class="wrap-mgi">
							<div class="mgi-sign announce"></div>
							<div class="mgi-text">ประกาศ</div>
						</div>
						<div class="menu-list stretch">
							<div class="wrap-menu-list">
								<?php for ($i=0; $i < 2; $i++) { ?>
									<div class="ml-item">
										<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
											<img src="<?=WEB_META_BASE_URL?>images/email.svg" alt="">
										</span><span style="display: inline-block;vertical-align: middle;">
											<span class="ft-text-green-80">
												E-Mail แจ้งเตือน
											</span><br/>
											<span class="ft-text-grey-8">
												XXXXXXXXXX
											</span>
										</span>
									</div>
									<div class="ml-item">
										<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
											<img src="<?=WEB_META_BASE_URL?>images/msg.svg" alt="">
										</span><span style="display: inline-block;vertical-align: middle;">
											<span class="ft-text-green-80">
												SMS แจ้งเตือน
											</span><br/>
											<span class="ft-text-grey-8">
												XXXXXXXXXX
											</span>
										</span>
									</div>
									<div class="ml-item">
										<span style="display: inline-block;vertical-align: middle;margin-right: 15px;">
											<img src="<?=WEB_META_BASE_URL?>images/alert.svg" alt="">
										</span><span style="display: inline-block;vertical-align: middle;">
											<span class="ft-text-green-80">
												Alert. !!!
											</span><br/>
											<span class="ft-text-grey-8">
												XXXXXXXXXX
											</span>
										</span>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="mg-item">
						<div class="mgi-sign list"></div>
						<div class="mgi-text">ใบสั่งชำระ</div>
					</div>
					<div class="mg-item mg-alert">
						<div class="mgi-sign system"></div>
						<div class="mgi-text">ระบบงานอื่น</div>
						<div class="menu-list">
							<div class="wrap-menu-list">
								<?php for ($i=0; $i < 30; $i++) { ?>
								<div class="ml-item">
									<p><img src="<?=WEB_META_BASE_URL?>images/menu-list-example.svg" alt=""></p>
									<p class="ft-text-green-80">ระบบ <?=$i+1?></p>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="mg-item">
						<div class="mgi-sign noti"></div>
						<div class="mgi-text">แจ้งเตือน</div>
					</div>
					<div class="mg-item">
						<div class="mgi-sign company"></div>
						<div class="mgi-text s-16">
							Products Profile<br>บ. ขายยาดี จำกัด
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-body">
		<div class="ft-breadcrumb">
			<div class="wrap-ft-breadcrumb">
				<a href="<?=WEB_META_BASE_URL?>"><span class="sign home"><i class="fas fa-home"></i></span><span class="txt">หน้าแรก</span></a>
			</div>
		</div>
		<div class="ft-container ft-3-part">
			<div class="ft-aside-l">
				<div class="ft-content bg-white-100">
				  <div class="ft-block">
				  	<div class="site-nav ft-h4">
				  		<span class="sign"></span><span class="txt">Site Navigation</span>
				  	</div>
				  	<?php
				  		$menu = array(
				  			array(
				  				"link"=>WEB_META_BASE_URL,
				  				"text"=>"Getting Started",
				  			),	
				  			array(
				  				"link"=>WEB_META_BASE_URL.'snippet.php',
				  				"text"=>"Snippet UI Kit",
				  				"child"=> array(
				  					"grid-system"=>"Grid System",
				  					"heading"=>"Headings and paragraphs",
				  					"colors"=>"Colors",
				  					"badges"=>"Badges",
				  					"table"=>"Table",
				  					"form"=>"Form",
				  					"listing"=>"Listing",
				  					"card"=>"Card",
				  				),
				  			),	
				  			array(
				  				"link"=>WEB_META_BASE_URL.'layout.php',
				  				"text"=>"Layout",
				  			),	
				  			array(
				  				"link"=>WEB_META_BASE_URL.'mobile_tab.php',
				  				"text"=>"Mobile Tab Layout",
				  			),	
				  			array(
				  				"link"=>WEB_META_BASE_URL.'menu.php',
				  				"text"=>"Menu",
				  			),
				  		);
				  	?>
				  	<ul class="ft-list">
				  		<?php
				  			foreach ($menu as $k => $v) {
				  				$active = "";
				  				if(strpos($v["link"], $uri) || ($uri=="" && $v["link"]==WEB_META_BASE_URL)){
				  					$active = "bold";
				  				}
				  				$html = '<li class="ft-h6">
				  						<a class="'.$active.' ft-text-green-80" href="'.$v["link"].'"><i class="fas fa-chevron-right"></i> '.$v["text"].'</a>';
				  				if($active!="" && isset($v["child"])){
				  					$html.='<ul class="ft-list" style="padding-left: 15px;margin-top: 10px;">';
				  					foreach ($v["child"] as $kk => $vv) {
				  						$html.='<li class="ft-h6"><a class="goto" style="color: #333" data-goto="#'.$kk.'"><i class="fas fa-chevron-right"></i> '.$vv.'</a></li>';
				  					}
				  					$html.='</ul>';
				  				}
				  				$html.='</li>';
				  				echo $html;
				  			}
				  		?>
				  	</ul>
				  </div>
				</div>
			</div>
			<div class="ft-main">