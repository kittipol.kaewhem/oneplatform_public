<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL); // Show all errors & warning
	error_reporting(E_ERROR | E_PARSE); // Show only errors
	
	if ( ! defined( 'ABSPATH' ) ) {
		define( 'ABSPATH', dirname( __FILE__ ) . '/' );
	}

	define("WEB_SLUG", "/oneplatform_public/");

	$HTTP_REFERER = isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:'http://' . @$_SERVER["SERVER_NAME"] . WEB_SLUG;
	$HTTP_PROTOCAL = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")?"https":"http";
	list($HTTP_PROTOCAL) = explode(':', $HTTP_REFERER);

	define("WEB_REWRITE_BASE",  "");
	define("WEB_META_BASE_URL",  $HTTP_PROTOCAL."://" . @$_SERVER["SERVER_NAME"] .WEB_REWRITE_BASE. WEB_SLUG);


	$isexistheader = false;
	if( !function_exists('apache_request_headers') ) {
	///
	function apache_request_headers() {
	  $arh = array();
	  $rx_http = '/\AHTTP_/';
	  foreach($_SERVER as $key => $val) {
	    if( preg_match($rx_http, $key) ) {
	      $arh_key = preg_replace($rx_http, '', $key);
	      $rx_matches = array();
	      // do some nasty string manipulations to restore the original letter case
	      // this should work in most cases
	      $rx_matches = explode('_', $arh_key);
	      if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
	        foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
	        $arh_key = implode('-', $rx_matches);
	      }
	      $arh[$arh_key] = $val;
	    }
	  }
	  return( $arh );
	}
	///
	}else{
	  $isexistheader = true;
	}
	///
	define(ISEXISTHEADER, $isexistheader);
?>