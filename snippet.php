<?php include 'template/header.php';?>
<div class="ft-content">
    <div id="grid" class="ft-block">
        <h1 class="ft-title">Grid System</h1>
        <h2>Grid options</h2>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-center">
                        Extra small<br />
                        <small>&lt;576px</small>
                    </th>
                    <th class="text-center">
                        Small<br />
                        <small>≥576px</small>
                    </th>
                    <th class="text-center">
                        Medium<br />
                        <small>≥768px</small>
                    </th>
                    <th class="text-center">
                        Large<br />
                        <small>≥992px</small>
                    </th>
                    <th class="text-center">
                        Extra large<br />
                        <small>≥1200px</small>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-nowrap" scope="row">Max container width</th>
                    <td>None (auto)</td>
                    <td>540px</td>
                    <td>720px</td>
                    <td>960px</td>
                    <td>1140px</td>
                </tr>
                <tr>
                    <th class="text-nowrap" scope="row">Class prefix</th>
                    <td><code>.col-</code></td>
                    <td><code>.col-sm-</code></td>
                    <td><code>.col-md-</code></td>
                    <td><code>.col-lg-</code></td>
                    <td><code>.col-xl-</code></td>
                </tr>
                <tr>
                    <th class="text-nowrap" scope="row"># of columns</th>
                    <td colspan="5">12</td>
                </tr>
                <tr>
                    <th class="text-nowrap" scope="row">Gutter width</th>
                    <td colspan="5">30px (15px on each side of a column)</td>
                </tr>
                <tr>
                    <th class="text-nowrap" scope="row">Nestable</th>
                    <td colspan="5">Yes</td>
                </tr>
                <tr>
                    <th class="text-nowrap" scope="row">Column ordering</th>
                    <td colspan="5">Yes</td>
                </tr>
            </tbody>
        </table>
        <h2 class="ft-mt30 ft-heading2">Mix and match</h2>
        <div class="ft-example-row">
            <div class="ft-example">
                <div class="row">
                    <div class="col-12 col-md-8">.col-12 .col-md-8</div>
                    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                </div>
                <div class="row">
                    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                </div>
                <div class="row">
                    <div class="col-6">.col-6</div>
                    <div class="col-6">.col-6</div>
                </div>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8">.col-12 .col-md-8</div>
      <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    </div>
    <div class="row">
      <div class="col-6 col-md-4">.col-6 .col-md-4</div>
      <div class="col-6 col-md-4">.col-6 .col-md-4</div>
      <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    </div>
    <div class="row">
      <div class="col-6">.col-6</div>
      <div class="col-6">.col-6</div>
    </div>
  </div>
</pre>
        </div>
    </div>
    <!-- Headings and paragraphs -->
    <div id="heading" class="ft-block">
        <h1 class="ft-title ft-thin">Headings and paragraphs</h1>
        <div class="row">
            <div class="col-lg-6">
                <h1 class="ft-display1">Display 1 <span class="ft-remark">(6rem)</span></h1>
                <h1 class="ft-display2">Display 2 <span class="ft-remark">(5.5rem)</span></h1>
                <h1 class="ft-display3">Display 3 <span class="ft-remark">(4.5rem)</span></h1>
                <h1 class="ft-display4">Display 4 <span class="ft-remark">(3.4rem)</span></h1>
            </div>
            <div class="col-lg-6 mt-2">
                <p class="ft-h1">h1. heading <span class="ft-remark">(2.5rem)</span></p>
                <p class="ft-h2">h2. heading <span class="ft-remark">(2rem)</span></p>
                <p class="ft-h3">h3. heading <span class="ft-remark">(1.75rem)</span></p>
                <p class="ft-h4">h4. heading <span class="ft-remark">(1.5rem)</span></p>
                <p class="ft-h5">h5. heading <span class="ft-remark">(1.25rem)</span></p>
                <p class="ft-h6">h6. heading <span class="ft-remark">(0.95rem)</span></p>
                <p>paragraph <span class="ft-remark">(16px)</span></p>
                <p class="ft-remark">Remark <span class="ft-remark">(11px)</span></p>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <h1 class="ft-display1">Display 1 <span class="ft-remark">(6rem)</span></h1>
  <h1 class="ft-display2">Display 2 <span class="ft-remark">(5.5rem)</span></h1>
  <h1 class="ft-display3">Display 3 <span class="ft-remark">(4.5rem)</span></h1>
  <h1 class="ft-display4">Display 4 <span class="ft-remark">(3.4rem)</span></h1>
  <p class="ft-h1">h1. heading <span class="ft-remark">(2.5rem)</span></p>
  <p class="ft-h2">h2. heading <span class="ft-remark">(2rem)</span></p>
  <p class="ft-h3">h3. heading <span class="ft-remark">(1.75rem)</span></p>
  <p class="ft-h4">h4. heading <span class="ft-remark">(1.5rem)</span></p>
  <p class="ft-h5">h5. heading <span class="ft-remark">(1.25rem)</span></p>
  <p class="ft-h6">h6. heading <span class="ft-remark">(0.95rem)</span></p>
  <p>paragraph <span class="ft-remark">(16px)</span></p>
  <p class="ft-remark">Remark <span class="ft-remark">(11px)</span></p>
</pre>
        </div>
    </div>
    <!-- End Headings and paragraphs -->
    <!-- Colors -->
    <div id="colors" class="ft-block bg-grey-80">
        <h1 class="ft-title ft-thin">Colors</h1>
        <div class="row">
            <div class="col-lg-3">
                <h2>Pimary</h2>
                <div class="ft-box-bg ft-text-white-100 ft-bg-green-80">
                    .ft-bg-green-80<br />
                    <span class="ft-remark">#296838</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-green-100">
                    .ft-bg-green-100<br />
                    <span class="ft-remark">#1A5027</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-green-50">
                    .ft-bg-green-50<br />
                    <span class="ft-remark">#8CAF94</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-orange-100">
                    .ft-bg-orange-100<br />
                    <span class="ft-remark">#FF8900</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-orange-80">
                    .ft-bg-orange-80<br />
                    <span class="ft-remark">#E59D4B</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-orange-50">
                    .ft-bg-orange-50<br />
                    <span class="ft-remark">#FAC07D</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-danger-80">
                    .ft-bg-danger-80<br />
                    <span class="ft-remark">#D85A2C</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-danger-100">
                    .ft-bg-danger-100<br />
                    <span class="ft-remark">#AA4824</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-danger-50">
                    .ft-bg-danger-50<br />
                    <span class="ft-remark">#F59370</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-black-100">
                    .ft-bg-black-100<br />
                    <span class="ft-remark">#333333</span>
                </div>
                <div class="ft-box-bg ft-text-white-100 ft-bg-grey-100">
                    .ft-bg-grey-100<br />
                    <span class="ft-remark">#777777</span>
                </div>
                <div class="ft-box-bg ft-text-black-100 ft-bg-grey-80">
                    .ft-bg-grey-80<br />
                    <span class="ft-remark">#EDEDED</span>
                </div>
                <div class="ft-box-bg ft-text-black-100 ft-bg-white-100 border-bottom border-right border-left">
                    .ft-bg-white-100<br />
                    <span class="ft-remark">#FFFFFF</span>
                </div>
            </div>
            <div class="col-lg-9">
                <h2>Strategy</h2>
                <div class="text-center">
                    <img src="<?=WEB_META_BASE_URL?>images/strategy.png" class="img-fluid" alt="Strategy" />
                    <h2 class="mt-5 text-left">Status</h2>
                    <div class="row">
                        <div class="col-sm">
                            <div class="ft-bg ft-bg-green-60">.ft-bg-green-60<br /><span class="ft-remark">#2CD889</span></div>
                            <div class="text-left mt-2">Complete</div>
                        </div>
                        <div class="col-sm">
                            <div class="ft-bg ft-bg-danger-80 ft-text-white-100">.ft-bg-danger-80<br /><span class="ft-remark">#D85A2C</span></div>
                            <div class="text-left mt-2">Denined</div>
                        </div>
                        <div class="col-sm">
                            <div class="ft-bg ft-bg-orange-100 ft-text-white-100">.ft-bg-orange-100<br /><span class="ft-remark">#AA4824</span></div>
                            <div class="text-left mt-2">Waiting</div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm">
                            <div class="ft-bg ft-bg-blue-100 ft-text-white-100">.ft-bg-blue-100<br /><span class="ft-remark">#2C75D8</span></div>
                            <div class="text-left mt-2">Pending</div>
                        </div>
                        <div class="col-sm">
                            <div class="ft-bg ft-bg-gray-50">.ft-bg-gray-50<br /><span class="ft-remark">#CECFD0</span></div>
                            <div class="text-left mt-2">Not complete</div>
                        </div>
                    </div>
                </div>
                <div class="ft-code-example">
                    <pre class="code" lang="html">
    <div class="ft-bg ft-bg-green-60">.ft-bg-green-60</div>
    <div class="ft-bg ft-bg-danger-80">.ft-bg-danger-80</div>
    <div class="ft-bg ft-bg-orange-100">.ft-bg-orange-100</div>
    <div class="ft-bg ft-bg-blue-100">.ft-bg-blue-100</div>
    <div class="ft-bg ft-bg-gray-50">.ft-bg-gray-50</div></pre>
                </div>
                <h2 class="mt-5">Text Colors</h2>
                <div class="ft-code-example">
                    <pre class="code" lang="html">
    <p class="ft-text-green-80">.ft-text-green-80</p>
    <p class="ft-text-green-100">.ft-text-green-100</p>
    <p class="ft-text-green-50">.ft-text-green-50</p>
    <p class="ft-text-orange-100">.ft-text-orange-100</p>
    <p class="ft-text-orange-80">.ft-text-orange-80</p>
    <p class="ft-text-orange-50">.ft-text-orange-50</p>
    <p class="ft-text-danger-80">.ft-text-danger-80</p>
    <p class="ft-text-danger-100">.ft-text-danger-100</p>
    <p class="ft-text-danger-50">.ft-text-danger-50</p>
    <p class="ft-text-black-100">.ft-text-black-100</p>
    <p class="ft-text-grey-100">.ft-text-grey-100</p>
    <p class="ft-text-grey-80">.ft-text-grey-80</p>
    <p class="ft-text-white-100">.ft-text-white-100</p></pre>
                </div>
            </div>
        </div>
    </div>
    <!-- End Colors -->
    <!-- Badges -->
    <div id="badges" class="ft-block">
        <h1 class="ft-title ft-thin">Badges</h1>
        <div class="row">
            <div class="col-lg-6">
                <h6 class="grey_100">Examples</h6>
                <h1>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h1>
                <h2>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h2>
                <h3>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h3>
                <h4>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h4>
                <h5>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h5>
                <h6>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h6>
            </div>
            <div class="col-lg-6">
                <h6 class="grey_100">Contextual Badges</h6>
                <span class="ft-badge ft-bg-green-80 ft-text-white-100">Primary</span>
                <span class="ft-badge ft-bg-orange-100 ft-text-white-100">Secondary</span>
                <span class="ft-badge ft-bg-green-60 ft-text-white-100">Success</span>
                <span class="ft-badge ft-bg-danger-80 ft-text-white-100">Danger</span>
                <span class="ft-badge ft-bg-orange-80 ft-text-white-100">Warning</span>
                <span class="ft-badge ft-bg-blue-100 ft-text-white-100">Info</span>
                <span class="ft-badge ft-bg-white-100">Light</span>
                <span class="ft-badge ft-bg-black-100 ft-text-white-100">Dark</span>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <h1>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h1>
  <h2>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h2>
  <h3>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h3>
  <h4>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h4>
  <h5>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h5>
  <h6>Example heading <span class="ft-badge ft-bg-grey-100 ft-text-white-100">New</span></h6>

  <span class="ft-badge ft-bg-green-80 ft-text-white-100">Primary</span>
  <span class="ft-badge ft-bg-orange-100 ft-text-white-100">Secondary</span>
  <span class="ft-badge ft-bg-green-60 ft-text-white-100">Success</span>
  <span class="ft-badge ft-bg-danger-80 ft-text-white-100">Danger</span>
  <span class="ft-badge ft-bg-orange-80 ft-text-white-100">Warning</span>
  <span class="ft-badge ft-bg-blue-100 ft-text-white-100">Info</span>
  <span class="ft-badge ft-bg-white-100">Light</span>
  <span class="ft-badge ft-bg-black-100 ft-text-white-100">Dark</span>
</pre>
        </div>
    </div>
    <!-- End Badges -->
    <!-- Table -->
    <div id="table" class="ft-block">
        <h1 class="ft-title ft-thin">Table</h1>
        <h2>Examples</h2>
        <table class="ft-table">
            <thead>
                <tr>
                    <th class="ft-h5" scope="col">#</th>
                    <th class="ft-h5" scope="col">First</th>
                    <th class="ft-h5" scope="col">Last</th>
                    <th class="ft-h5" scope="col">Handle</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                </tr>
            </tbody>
        </table>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <table class="ft-table">
    <thead>
     <tr>
        <th class="ft-h5" scope="col">#</th>
        <th class="ft-h5" scope="col">First</th>
        <th class="ft-h5" scope="col">Last</th>
        <th class="ft-h5" scope="col">Handle</th>
     </tr>
    </thead>
    <tbody>
     <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
        <td>@mdo</td>
     </tr>
     <tr>
        <th scope="row">2</th>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>@fat</td>
     </tr>
     <tr>
        <th scope="row">3</th>
        <td>Larry</td>
        <td>the Bird</td>
        <td>@twitter</td>
     </tr>
    </tbody>
  </table>
</pre>
        </div>
        <table class="ft-table mt-5">
            <thead>
                <tr>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">แก้ไข</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">ลำดับ</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">ชื่อ</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">ผู้ตรวจ</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">ผลิตภัณฑ์</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">วันที่อัพเดท</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">สถานะ</p></th>
                    <th scope="col"><p class="ft-h6 ft-text-grey-100">คำสั่ง</p></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="ft-text-green-80">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">101</td>
                    <td class="ft-text-black-100">Inverness McKenzie</td>
                    <td class="ft-text-green-100">Dr.Lance Bogrol</td>
                    <td class="ft-text-black-100 font-weight-bold">Influenza</td>
                    <td class="ft-text-black-100">24.May.20</td>
                    <td>
                        <div class="ft-bg ft-bg-orange-100 ft-text-white-100">รอพิจารณา</div>
                    </td>
                    <td class="ft-text-orange-100">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M1.643 3.143L.427 1.927A.25.25 0 000 2.104V5.75c0 .138.112.25.25.25h3.646a.25.25 0 00.177-.427L2.715 4.215a6.5 6.5 0 11-1.18 4.458.75.75 0 10-1.493.154 8.001 8.001 0 101.6-5.684zM7.75 4a.75.75 0 01.75.75v2.992l2.028.812a.75.75 0 01-.557 1.392l-2.5-1A.75.75 0 017 8.25v-3.5A.75.75 0 017.75 4z"
                            ></path>
                        </svg>
                        ติดตาม
                    </td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">102</td>
                    <td class="ft-text-black-100">Theodore Handle</td>
                    <td class="ft-text-green-100">Dr.Alan Fresco</td>
                    <td class="ft-text-black-100 font-weight-bold">Cholera</td>
                    <td class="ft-text-black-100">31.May.20</td>
                    <td>
                        <div class="ft-bg ft-bg-danger-80 ft-text-white-100">มีแก้ไข</div>
                    </td>
                    <td class="ft-text-danger-100">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                        ขอคำชี้แจง
                    </td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">103</td>
                    <td class="ft-text-black-100">Niles Peppertrout</td>
                    <td class="ft-text-green-100">Dr.Indigo Violet</td>
                    <td class="ft-text-black-100 font-weight-bold">Jaundice</td>
                    <td class="ft-text-black-100">17.Jun.20</td>
                    <td>
                        <div class="ft-bg ft-bg-green-60 ft-text-white-100">ชำระแล้ว</div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">104</td>
                    <td class="ft-text-black-100">Valentino Morose</td>
                    <td class="ft-text-green-100">Dr.Justin Case</td>
                    <td class="ft-text-black-100 font-weight-bold">Influenza</td>
                    <td class="ft-text-black-100">19.Aug.20</td>
                    <td>
                        <div class="ft-bg ft-bg-green-60 ft-text-white-100">ชำระแล้ว</div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">105</td>
                    <td class="ft-text-black-100">Abraham Pigeon</td>
                    <td class="ft-text-green-100">Dr.Hilary Ouse</td>
                    <td class="ft-text-black-100 font-weight-bold">Diahoria</td>
                    <td class="ft-text-black-100">7.Oct.20</td>
                    <td>
                        <div class="ft-bg ft-bg-blue-100 ft-text-white-100">รอแนบไฟล์</div>
                    </td>
                    <td class="ft-text-blue-100">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M3.75 1.5a.25.25 0 00-.25.25v11.5c0 .138.112.25.25.25h8.5a.25.25 0 00.25-.25V6H9.75A1.75 1.75 0 018 4.25V1.5H3.75zm5.75.56v2.19c0 .138.112.25.25.25h2.19L9.5 2.06zM2 1.75C2 .784 2.784 0 3.75 0h5.086c.464 0 .909.184 1.237.513l3.414 3.414c.329.328.513.773.513 1.237v8.086A1.75 1.75 0 0112.25 15h-8.5A1.75 1.75 0 012 13.25V1.75z"
                            ></path>
                        </svg>
                        แนบไฟล์เพิ่มเติม
                    </td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">106</td>
                    <td class="ft-text-black-100">Ingredia Nutrisha</td>
                    <td class="ft-text-green-100">Dr.Archibald</td>
                    <td class="ft-text-black-100 font-weight-bold">Corona</td>
                    <td class="ft-text-black-100">4.Nov.20</td>
                    <td>
                        <div class="ft-bg ft-bg-gray-50 ft-text-white-100">ยังไม่เสร็จ</div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">107</td>
                    <td class="ft-text-black-100">Inverness McKenzie</td>
                    <td class="ft-text-green-100">Dr.Lance Bogrol</td>
                    <td class="ft-text-black-100 font-weight-bold">Influenza</td>
                    <td class="ft-text-black-100">24.May.20</td>
                    <td>
                        <div class="ft-bg ft-bg-gray-50 ft-text-white-100">ยังไม่เสร็จ</div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                            ></path>
                        </svg>
                    </td>
                    <td class="ft-text-black-100">108</td>
                    <td class="ft-text-black-100">Inverness McKenzie</td>
                    <td class="ft-text-green-100">Dr.Lance Bogrol</td>
                    <td class="ft-text-black-100 font-weight-bold">Influenza</td>
                    <td class="ft-text-black-100">24.May.20</td>
                    <td>
                        <div class="ft-bg ft-bg-orange-100 ft-text-white-100">รอพิจารณา</div>
                    </td>
                    <td class="ft-text-orange-100">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M1.643 3.143L.427 1.927A.25.25 0 000 2.104V5.75c0 .138.112.25.25.25h3.646a.25.25 0 00.177-.427L2.715 4.215a6.5 6.5 0 11-1.18 4.458.75.75 0 10-1.493.154 8.001 8.001 0 101.6-5.684zM7.75 4a.75.75 0 01.75.75v2.992l2.028.812a.75.75 0 01-.557 1.392l-2.5-1A.75.75 0 017 8.25v-3.5A.75.75 0 017.75 4z"
                            ></path>
                        </svg>
                        ติดตาม
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <table class="ft-table mt-5">
    <thead>
     <tr>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">แก้ไข</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">ลำดับ</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">ชื่อ</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">ผู้ตรวจ</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">ผลิตภัณฑ์</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">วันที่อัพเดท</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">สถานะ</p></th>
        <th scope="col"><p class="ft-h6 ft-text-grey-100">คำสั่ง</p></th>
     </tr>
    </thead>
    <tbody>
     <tr>
        <td class="ft-text-green-80">
           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"></path>
           </svg>
        </td>
        <td class="ft-text-black-100">101</td>
        <td class="ft-text-black-100">Inverness McKenzie</td>
        <td class="ft-text-green-100">Dr.Lance Bogrol</td>
        <td class="ft-text-black-100 font-weight-bold">Influenza</td>
        <td class="ft-text-black-100">24.May.20</td>
        <td>
           <div class="ft-bg ft-bg-orange-100 ft-text-white-100">รอพิจารณา</div>
        </td>
        <td class="ft-text-orange-100">
           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M1.643 3.143L.427 1.927A.25.25 0 000 2.104V5.75c0 .138.112.25.25.25h3.646a.25.25 0 00.177-.427L2.715 4.215a6.5 6.5 0 11-1.18 4.458.75.75 0 10-1.493.154 8.001 8.001 0 101.6-5.684zM7.75 4a.75.75 0 01.75.75v2.992l2.028.812a.75.75 0 01-.557 1.392l-2.5-1A.75.75 0 017 8.25v-3.5A.75.75 0 017.75 4z"></path>
           </svg>
           ติดตาม
        </td>
     </tr>
     <tr>
        <td>
           <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"></path>
           </svg>
        </td>
        <td class="ft-text-black-100">102</td>
        <td class="ft-text-black-100">Theodore Handle</td>
        <td class="ft-text-green-100">Dr.Alan Fresco</td>
        <td class="ft-text-black-100 font-weight-bold">Cholera</td>
        <td class="ft-text-black-100">31.May.20</td>
        <td>
           <div class="ft-bg ft-bg-danger-80 ft-text-white-100">มีแก้ไข</div>
        </td>
        <td class="ft-text-danger-100">
           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"></path>
           </svg>
           ขอคำชี้แจง
        </td>
     </tr>
     <tr>
        <td>
           <svg class="ft-text-green-80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"></path>
           </svg>
        </td>
        <td class="ft-text-black-100">103</td>
        <td class="ft-text-black-100">Niles Peppertrout</td>
        <td class="ft-text-green-100">Dr.Indigo Violet</td>
        <td class="ft-text-black-100 font-weight-bold">Jaundice</td>
        <td class="ft-text-black-100">17.Jun.20</td>
        <td>
           <div class="ft-bg ft-bg-green-60 ft-text-white-100">ชำระแล้ว</div>
        </td>
        <td></td>
     </tr>
    </tbody>
  </table>
</pre>
        </div>
    </div>
    <!-- End Table -->
    <!-------------------- Form -------------------->
    <div id="form" class="ft-block">
        <h1 class="ft-title">Form</h1>
        <div class="row">
            <div class="col-lg-6">
                <h6 class="h6 ft-text-grey-100 mb-4">Example</h6>
                <form>
                    <div class="ft-form-group">
                        <label class="ft-text-grey-100">Email address</label>
                        <input type="email" class="ft-form-control" placeholder="Enter email" />
                        <small class="ft-form-text ft-text-grey-100">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="ft-form-group">
                        <label class="ft-text-grey-100">Password</label>
                        <input type="password" class="ft-form-control" placeholder="Password" />
                    </div>
                    <div class="ft-form-check">
                        <input type="checkbox" class="ft-form-check-input" />
                        <label class="ft-check-label ft-text-grey-100">Check me out</label>
                    </div>
                    <button type="submit" class="ft-btn ft-btn-primary mt-2">Submit</button>
                </form>
            </div>
            <div class="col-lg-6">
                <h6 class="h6 ft-text-grey-100 mb-4">Form Controls</h6>
                <form>
                    <div class="ft-form-group">
                        <label class="ft-text-grey-100">Email address</label>
                        <input type="email" class="ft-form-control" placeholder="name@example.com" />
                    </div>
                    <div class="ft-form-group">
                        <label class="ft-text-grey-100">Example select</label>
                        <select class="ft-form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="ft-form-group">
                        <label class="ft-text-grey-100">Example multiple select</label>
                        <select multiple class="ft-form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="ft-form-group">
                        <label class="ft-text-grey-100">Example textarea</label>
                        <textarea class="ft-form-control" rows="3"></textarea>
                    </div>
                </form>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <!---------- Example ---------->
  <form>
    <div class="ft-form-group">
      <label class="ft-text-grey-100">Email address</label>
      <input type="email" class="ft-form-control" placeholder="Enter email">
      <small class="ft-form-text ft-text-grey-100">We'll never share your email with anyone else.</small>
    </div>
    <div class="ft-form-group">
      <label class="ft-text-grey-100">Password</label>
      <input type="password" class="ft-form-control" placeholder="Password">
    </div>
    <div class="ft-form-check">
      <input type="checkbox" class="ft-form-check-input">
      <label class="ft-check-label ft-text-grey-100">Check me out</label>
    </div>
    <button type="submit" class="ft-btn ft-btn-primary mt-2">Submit</button>

    <!---------- Form Controls ---------->
    <div class="ft-form-group">
      <label class="ft-text-grey-100">Email address</label>
      <input type="email" class="ft-form-control" placeholder="name@example.com">
    </div>
    <div class="ft-form-group">
      <label class="ft-text-grey-100">Example select</label>
      <select class="ft-form-control">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </select>
    </div>
    <div class="ft-form-group">
      <label class="ft-text-grey-100">Example multiple select</label>
      <select multiple class="ft-form-control">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </select>
    </div>
    <div class="ft-form-group">
      <label class="ft-text-grey-100">Example textarea</label>
      <textarea class="ft-form-control" rows="3"></textarea>
    </div>
  </form>
</pre>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6">
                <h6 class="h6 ft-text-grey-100 mb-4">Sizing</h6>
                <input class="ft-form-control ft-form-lg" type="text" placeholder=".ft-form-lg" />
                <input class="ft-form-control mt-2" type="text" placeholder="Default input" />
                <input class="ft-form-control mt-2 ft-form-sm" type="text" placeholder=".ft-form-sm" />
                <select class="ft-form-control ft-form-lg mt-3">
                    <option>Large select</option>
                </select>
                <select class="ft-form-control mt-2">
                    <option>Default select</option>
                </select>
                <select class="ft-form-control mt-2 ft-form-sm">
                    <option>Small select</option>
                </select>
            </div>
            <div class="col-lg-6">
                <h6 class="h6 ft-text-grey-100 mb-4">Readonly</h6>
                <input class="ft-form-control" type="text" placeholder="Readonly input here…" readonly />
                <br />
                <br />
                <h6 class="h6 ft-text-grey-100 mb-4">Readonly plain text</h6>
                <form class="mt-2">
                    <div class="ft-form-group row">
                        <label class="col-sm-2 ft-col-form-label ft-text-grey-100">Email</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="ft-form-control-plaintext" value="email@example.com" />
                        </div>
                    </div>
                    <div class="ft-form-group row">
                        <label class="col-sm-2 ft-col-form-label ft-text-grey-100">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="ft-form-control" placeholder="Password" />
                        </div>
                    </div>
                </form>
                <form class="ft-form-inline">
                    <div class="ft-form-group mb-2">
                        <label class="ft-sr-only ft-text-grey-100">Email</label>
                        <input type="text" readonly class="ft-form-control-plaintext" value="email@example.com" />
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label class="ft-sr-only ft-text-grey-100">Password</label>
                        <input type="password" class="ft-form-control" placeholder="Password" />
                    </div>
                    <button type="submit" class="ft-btn ft-btn-primary mb-2">Submit</button>
                </form>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <!---------- Sizing ---------->
  <input class="ft-form-control ft-form-lg" type="text" placeholder=".ft-form-lg">
  <input class="ft-form-control" type="text" placeholder="Default input">
  <input class="ft-form-control ft-form-sm" type="text" placeholder=".ft-form-sm">
  <select class="ft-form-control ft-form-lg">
    <option>Large select</option>
  </select>
  <select class="ft-form-control">
    <option>Default select</option>
  </select>
  <select class="ft-form-control ft-form-sm">
    <option>Small select</option>
  </select>

  <!---------- Readonly ---------->
  <input class="ft-form-control" type="text" placeholder="Readonly input here…" readonly>
  
  <!---------- Readonly plain text ---------->
  <div class="ft-form-group row">
    <label class="col-sm-2 ft-col-form-label ft-text-grey-100">Email</label>
    <div class="col-sm-10">
      <input type="text" readonly class="ft-form-control-plaintext" value="email@example.com">
    </div>
  </div>
  <div class="ft-form-group row">
    <label class="col-sm-2 ft-col-form-label ft-text-grey-100">Password</label>
    <div class="col-sm-10">
      <input type="password" class="ft-form-control" placeholder="Password">
    </div>
  </div>
  <div class="ft-form-inline">
    <div class="ft-form-group mb-2">
      <label class="ft-sr-only ft-text-grey-100">Email</label>
      <input type="text" readonly class="ft-form-control-plaintext" value="email@example.com">
    </div>
    <div class="form-group mx-sm-3 mb-2">
      <label class="ft-sr-only ft-text-grey-100">Password</label>
      <input type="password" class="ft-form-control" placeholder="Password">
    </div>
    <button type="submit" class="ft-btn ft-btn-primary mb-2">Submit</button>
  </div>
</pre>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3">
                <h6 class="h6 ft-text-grey-100 mb-4">Help text</h6>
                <div class="ft-form-group">
                    <label class="ft-text-grey-100">Password</label>
                    <input type="password" class="ft-form-control" aria-describedby="passwordHelpBlock" />
                    <small class="ft-form-text ft-text-grey-100">
                        Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
                    </small>
                </div>
                <div class="ft-form-group">
                    <label class="ft-text-grey-100">Label</label>
                    <select class="ft-form-control">
                        <option>Default select</option>
                    </select>
                    <small class="ft-form-text ft-text-grey-100">
                        Default text
                    </small>
                </div>
            </div>
            <div class="col-lg-3">
                <h6 class="h6 ft-text-grey-100 mb-4">Disabled forms</h6>
                <form>
                    <fieldset disabled>
                        <div class="ft-form-group">
                            <label class="ft-text-grey-100">Disabled input</label>
                            <input type="text" class="ft-form-control" placeholder="Disabled input" />
                        </div>
                        <div class="ft-form-group">
                            <label class="ft-text-grey-100">Disabled select menu</label>
                            <select class="ft-form-control">
                                <option>Disabled select</option>
                            </select>
                        </div>
                        <div class="ft-form-check">
                            <input class="ft-form-check-input" type="checkbox" disabled />
                            <label class="ft-form-check-label ft-text-grey-100">
                                Can't check this
                            </label>
                        </div>
                        <button type="submit" class="ft-btn ft-btn-primary">Submit</button>
                    </fieldset>
                </form>
            </div>
            <div class="col-lg-6">
                <h6 class="h6 ft-text-grey-100 mb-4">Validation</h6>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="ft-form-group">
                            <label class="ft-text-grey-100">Label</label>
                            <input type="text" class="ft-form-control ft-is-invalid" placeholder="Default input" required />
                            <div class="ft-invalid-feedback">
                                Error message.
                            </div>
                        </div>
                        <div class="ft-form-group">
                            <label class="ft-text-grey-100">Label</label>
                            <input type="text" class="ft-form-control ft-is-valid" placeholder="Default input" required />
                            <div class="ft-valid-feedback">
                                Success!
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ft-form-group">
                            <label class="ft-text-grey-100">Table</label>
                            <input type="text" class="ft-form-control ft-is-invalid" placeholder="Default input" required />
                            <div class="ft-invalid-feedback">
                                Error message.
                            </div>
                        </div>
                        <div class="ft-form-group">
                            <label class="ft-text-grey-100">Label</label>
                            <select class="ft-form-control ft-custom-select ft-is-invalid">
                                <option>Default select</option>
                            </select>
                            <div class="ft-invalid-feedback">
                                Error message
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <!---------- Help text ---------->
  <div class="ft-form-group">
    <label class="ft-text-grey-100">Password</label>
    <input type="password" class="ft-form-control" aria-describedby="passwordHelpBlock">
    <small class="ft-form-text ft-text-grey-100">
      Your password must be 8-20 characters long, contain letters and numbers, 
      and must not contain spaces, special characters, or emoji.
    </small>
  </div>
  <div class="ft-form-group">
    <label class="ft-text-grey-100">Label</label>
    <select class="ft-form-control">
      <option>Default select</option>
    </select>
    <small class="ft-form-text ft-text-grey-100">
      Default text
    </small>
  </div>

  <!---------- Disabled forms ---------->
  <form>
    <fieldset disabled>
      <div class="ft-form-group">
        <label class="ft-text-grey-100">Disabled input</label>
        <input type="text" class="ft-form-control" placeholder="Disabled input">
      </div>
      <div class="ft-form-group">
        <label class="ft-text-grey-100">Disabled select menu</label>
        <select class="ft-form-control">
          <option>Disabled select</option>
        </select>
      </div>
      <div class="ft-form-check">
        <input class="ft-form-check-input" type="checkbox" disabled>
        <label class="ft-form-check-label ft-text-grey-100">
          Can't check this
        </label>
      </div>
      <button type="submit" class="ft-btn ft-btn-primary">Submit</button>
    </fieldset>
  </form>

  <!---------- Validation ---------->
  <div class="ft-form-group">
    <label class="ft-text-grey-100">Label</label>
    <input type="text" class="ft-form-control ft-is-invalid" placeholder="Default input" required>
    <div class="ft-invalid-feedback">
      Error message.
    </div>
  </div>
  <div class="ft-form-group">
    <label class="ft-text-grey-100">Label</label>
    <input type="text" class="ft-form-control ft-is-valid" placeholder="Default input" required>
    <div class="ft-valid-feedback">
      Success!
    </div>
  </div>
  <div class="ft-form-group">
    <label class="ft-text-grey-100">Table</label>
    <input type="text" class="ft-form-control ft-is-invalid" placeholder="Default input" required>
    <div class="ft-invalid-feedback">
      Error message.
    </div>
  </div>
  <div class="ft-form-group">
    <label class="ft-text-grey-100">Label</label>
    <select class="ft-form-control ft-custom-select ft-is-invalid">
      <option>Default select</option>
    </select>
    <div class="ft-invalid-feedback">
      Error message
    </div>
  </div>
</pre>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4">
                <h6 class="h6 ft-text-grey-100">
                    Checkboxes / Radios /<br />
                    Switches
                </h6>
            </div>
            <div class="col-lg-4">
                <h6 class="h6 ft-text-grey-100">Inline</h6>
                <div class="mb-2">
                    <div class="ft-form-check ft-form-check-inline">
                        <input class="ft-form-check-input" type="checkbox" value="option1" />
                        <label class="ft-check-label">1</label>
                    </div>
                    <div class="ft-form-check ft-form-check-inline">
                        <input class="ft-form-check-input" type="checkbox" value="option2" />
                        <label class="ft-check-label">2</label>
                    </div>
                    <div class="ft-form-check ft-form-check-inline">
                        <input class="ft-form-check-input" type="checkbox" value="option3" disabled />
                        <label class="ft-check-label">3 (disabled)</label>
                    </div>
                </div>
                <div>
                    <div class="ft-form-check ft-form-check-inline">
                        <input class="ft-form-check-input" type="radio" value="option1" />
                        <label class="ft-check-label">1</label>
                    </div>
                    <div class="ft-form-check ft-form-check-inline">
                        <input class="ft-form-check-input" type="radio" value="option2" />
                        <label class="ft-check-label">2</label>
                    </div>
                    <div class="ft-form-check ft-form-check-inline">
                        <input class="ft-form-check-input" type="radio" value="option3" disabled />
                        <label class="ft-check-label">3 (disabled)</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <h6 class="h6 ft-text-grey-100">Without labels</h6>
                <div class="ft-form-check mb-2">
                    <input class="ft-form-check-input ft-position-static" type="checkbox" value="option1" />
                </div>
                <div class="ft-form-check">
                    <input class="ft-form-check-input ft-position-static" type="radio" name="blankRadio" value="option1" />
                </div>
            </div>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <!---------- Inline ---------->
  <div class="ft-form-check ft-form-check-inline">
    <input class="ft-form-check-input" type="checkbox" value="option1">
    <label class="ft-check-label">1</label>
  </div>
  <div class="ft-form-check ft-form-check-inline">
    <input class="ft-form-check-input" type="checkbox" value="option2">
    <label class="ft-check-label">2</label>
  </div>
  <div class="ft-form-check ft-form-check-inline">
    <input class="ft-form-check-input" type="checkbox" value="option3" disabled>
    <label class="ft-check-label">3 (disabled)</label>
  </div>

  <!---------- Without labels ---------->
  <div class="ft-form-check">
    <input class="ft-form-check-input ft-position-static" type="checkbox" value="option1">
  </div>
  <div class="ft-form-check">
    <input class="ft-form-check-input ft-position-static" type="radio" name="blankRadio" value="option1">
  </div>
</pre>
        </div>
    </div>
    <!-------------------- End Form -------------------->
    <!-------------------- Listing -------------------->
    <div id="listing" class="ft-block">
        <h1 class="ft-title">Listing</h1>
        <div class="ft-listing-group">
            <div class="ft-remark ft-text-grey-100 mb-2">หมวดหมู่เอกสาร > ผลิตภัณฑ์ > หน่วยงาน</div>
            <p class="ft-h5 mb-2">
                <a class="ft-text-green-80" href="#" title="ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX">
                    <svg class="ft-listing-heading-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                        <path
                            fill-rule="evenodd"
                            d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                        ></path>
                    </svg>
                    ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX
                </a>
            </p>
            <span class="ft-badge ft-bg-orange-100 ft-text-white-100">รอพิจารณา</span>
            <p class="mt-2">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.
            </p>
            <ul class="ft-listing-inline">
                <li class="ft-h6">
                    <a class="ft-text-green-80" href="#" title="พิมพ์ใบสั่งชำระ">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M3.75 1.5a.25.25 0 00-.25.25v11.5c0 .138.112.25.25.25h8.5a.25.25 0 00.25-.25V6H9.75A1.75 1.75 0 018 4.25V1.5H3.75zm5.75.56v2.19c0 .138.112.25.25.25h2.19L9.5 2.06zM2 1.75C2 .784 2.784 0 3.75 0h5.086c.464 0 .909.184 1.237.513l3.414 3.414c.329.328.513.773.513 1.237v8.086A1.75 1.75 0 0112.25 15h-8.5A1.75 1.75 0 012 13.25V1.75z"
                            ></path>
                        </svg>
                        <span>พิมพ์ใบสั่งชำระ</span>
                    </a>
                </li>
                <li class="ft-h6">
                    <a class="ft-text-blue-100" href="#" title="แนบไฟล์เพิ่มเติม">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M2.75 1.5a.25.25 0 00-.25.25v12.5c0 .138.112.25.25.25h10.5a.25.25 0 00.25-.25V4.664a.25.25 0 00-.073-.177l-2.914-2.914a.25.25 0 00-.177-.073H2.75zM1 1.75C1 .784 1.784 0 2.75 0h7.586c.464 0 .909.184 1.237.513l2.914 2.914c.329.328.513.773.513 1.237v9.586A1.75 1.75 0 0113.25 16H2.75A1.75 1.75 0 011 14.25V1.75zm7 1.5a.75.75 0 01.75.75v1.5h1.5a.75.75 0 010 1.5h-1.5v1.5a.75.75 0 01-1.5 0V7h-1.5a.75.75 0 010-1.5h1.5V4A.75.75 0 018 3.25zm-3 8a.75.75 0 01.75-.75h4.5a.75.75 0 010 1.5h-4.5a.75.75 0 01-.75-.75z"
                            ></path>
                        </svg>
                        <span>แนบไฟล์เพิ่มเติม</span>
                    </a>
                </li>
                <li class="ft-h6">
                    <a class="ft-text-orange-100" href="#" title="แนบไฟล์เพิ่มเติม">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path fill-rule="evenodd" d="M1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0zM8 0a8 8 0 100 16A8 8 0 008 0zm.5 4.75a.75.75 0 00-1.5 0v3.5a.75.75 0 00.471.696l2.5 1a.75.75 0 00.557-1.392L8.5 7.742V4.75z"></path>
                        </svg>
                        <span>ติดตาม</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="ft-listing-group">
            <div class="ft-remark ft-text-grey-100 mb-2">หมวดหมู่เอกสาร > ผลิตภัณฑ์ > หน่วยงาน</div>
            <p class="ft-h5 mb-2">
                <a class="ft-text-green-80" href="#" title="ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX">
                    <svg class="ft-listing-heading-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                        <path
                            fill-rule="evenodd"
                            d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                        ></path>
                    </svg>
                    ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX
                </a>
            </p>
            <span class="ft-badge ft-bg-orange-100 ft-text-white-100">รอพิจารณา</span>
            <p class="mt-2">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.
            </p>
            <ul class="ft-listing-inline">
                <li class="ft-h6">
                    <a class="ft-text-green-80" href="#" title="พิมพ์ใบสั่งชำระ">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M3.75 1.5a.25.25 0 00-.25.25v11.5c0 .138.112.25.25.25h8.5a.25.25 0 00.25-.25V6H9.75A1.75 1.75 0 018 4.25V1.5H3.75zm5.75.56v2.19c0 .138.112.25.25.25h2.19L9.5 2.06zM2 1.75C2 .784 2.784 0 3.75 0h5.086c.464 0 .909.184 1.237.513l3.414 3.414c.329.328.513.773.513 1.237v8.086A1.75 1.75 0 0112.25 15h-8.5A1.75 1.75 0 012 13.25V1.75z"
                            ></path>
                        </svg>
                        <span>พิมพ์ใบสั่งชำระ</span>
                    </a>
                </li>
                <li class="ft-h6">
                    <a class="ft-text-blue-100" href="#" title="แนบไฟล์เพิ่มเติม">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M2.75 1.5a.25.25 0 00-.25.25v12.5c0 .138.112.25.25.25h10.5a.25.25 0 00.25-.25V4.664a.25.25 0 00-.073-.177l-2.914-2.914a.25.25 0 00-.177-.073H2.75zM1 1.75C1 .784 1.784 0 2.75 0h7.586c.464 0 .909.184 1.237.513l2.914 2.914c.329.328.513.773.513 1.237v9.586A1.75 1.75 0 0113.25 16H2.75A1.75 1.75 0 011 14.25V1.75zm7 1.5a.75.75 0 01.75.75v1.5h1.5a.75.75 0 010 1.5h-1.5v1.5a.75.75 0 01-1.5 0V7h-1.5a.75.75 0 010-1.5h1.5V4A.75.75 0 018 3.25zm-3 8a.75.75 0 01.75-.75h4.5a.75.75 0 010 1.5h-4.5a.75.75 0 01-.75-.75z"
                            ></path>
                        </svg>
                        <span>แนบไฟล์เพิ่มเติม</span>
                    </a>
                </li>
                <li class="ft-h6">
                    <a class="ft-text-orange-100" href="#" title="แนบไฟล์เพิ่มเติม">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path fill-rule="evenodd" d="M1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0zM8 0a8 8 0 100 16A8 8 0 008 0zm.5 4.75a.75.75 0 00-1.5 0v3.5a.75.75 0 00.471.696l2.5 1a.75.75 0 00.557-1.392L8.5 7.742V4.75z"></path>
                        </svg>
                        <span>ติดตาม</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="ft-listing-group">
            <div class="ft-remark ft-text-grey-100 mb-2">หมวดหมู่เอกสาร > ผลิตภัณฑ์ > หน่วยงาน</div>
            <p class="ft-h5 mb-2">
                <a class="ft-text-green-80" href="#" title="ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX">
                    <svg class="ft-listing-heading-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                        <path
                            fill-rule="evenodd"
                            d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"
                        ></path>
                    </svg>
                    ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX
                </a>
            </p>
            <span class="ft-badge ft-bg-orange-100 ft-text-white-100">รอพิจารณา</span>
            <p class="mt-2">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.
            </p>
            <ul class="ft-listing-inline">
                <li class="ft-h6">
                    <a class="ft-text-green-80" href="#" title="พิมพ์ใบสั่งชำระ">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M3.75 1.5a.25.25 0 00-.25.25v11.5c0 .138.112.25.25.25h8.5a.25.25 0 00.25-.25V6H9.75A1.75 1.75 0 018 4.25V1.5H3.75zm5.75.56v2.19c0 .138.112.25.25.25h2.19L9.5 2.06zM2 1.75C2 .784 2.784 0 3.75 0h5.086c.464 0 .909.184 1.237.513l3.414 3.414c.329.328.513.773.513 1.237v8.086A1.75 1.75 0 0112.25 15h-8.5A1.75 1.75 0 012 13.25V1.75z"
                            ></path>
                        </svg>
                        <span>พิมพ์ใบสั่งชำระ</span>
                    </a>
                </li>
                <li class="ft-h6">
                    <a class="ft-text-blue-100" href="#" title="แนบไฟล์เพิ่มเติม">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path
                                fill-rule="evenodd"
                                d="M2.75 1.5a.25.25 0 00-.25.25v12.5c0 .138.112.25.25.25h10.5a.25.25 0 00.25-.25V4.664a.25.25 0 00-.073-.177l-2.914-2.914a.25.25 0 00-.177-.073H2.75zM1 1.75C1 .784 1.784 0 2.75 0h7.586c.464 0 .909.184 1.237.513l2.914 2.914c.329.328.513.773.513 1.237v9.586A1.75 1.75 0 0113.25 16H2.75A1.75 1.75 0 011 14.25V1.75zm7 1.5a.75.75 0 01.75.75v1.5h1.5a.75.75 0 010 1.5h-1.5v1.5a.75.75 0 01-1.5 0V7h-1.5a.75.75 0 010-1.5h1.5V4A.75.75 0 018 3.25zm-3 8a.75.75 0 01.75-.75h4.5a.75.75 0 010 1.5h-4.5a.75.75 0 01-.75-.75z"
                            ></path>
                        </svg>
                        <span>แนบไฟล์เพิ่มเติม</span>
                    </a>
                </li>
                <li class="ft-h6">
                    <a class="ft-text-orange-100" href="#" title="แนบไฟล์เพิ่มเติม">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
                            <path fill-rule="evenodd" d="M1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0zM8 0a8 8 0 100 16A8 8 0 008 0zm.5 4.75a.75.75 0 00-1.5 0v3.5a.75.75 0 00.471.696l2.5 1a.75.75 0 00.557-1.392L8.5 7.742V4.75z"></path>
                        </svg>
                        <span>ติดตาม</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="ft-code-example">
            <pre class="code" lang="html">
  <div class="ft-listing-group">
    <div class="ft-remark ft-text-grey-100 mb-2">หมวดหมู่เอกสาร > ผลิตภัณฑ์ > หน่วยงาน</div>
    <p class="ft-h5 mb-2">
      <a class="ft-text-green-80" href="#" title="ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX">
        <svg class="ft-listing-heading-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"></path></svg> ชื่อเอกสาร XXXXXXXXXXXXXXXXXXXXXX
      </a>
    </p>
    <span class="ft-badge ft-bg-orange-100 ft-text-white-100">รอพิจารณา</span>
    <p class="mt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat.</p>
    <ul class="ft-listing-inline">
      <li class="ft-h6">
        <a class="ft-text-green-80" href="#" title="พิมพ์ใบสั่งชำระ">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M3.75 1.5a.25.25 0 00-.25.25v11.5c0 .138.112.25.25.25h8.5a.25.25 0 00.25-.25V6H9.75A1.75 1.75 0 018 4.25V1.5H3.75zm5.75.56v2.19c0 .138.112.25.25.25h2.19L9.5 2.06zM2 1.75C2 .784 2.784 0 3.75 0h5.086c.464 0 .909.184 1.237.513l3.414 3.414c.329.328.513.773.513 1.237v8.086A1.75 1.75 0 0112.25 15h-8.5A1.75 1.75 0 012 13.25V1.75z"></path></svg>
          <span>พิมพ์ใบสั่งชำระ</span>
        </a>
      </li>
      <li class="ft-h6">
        <a class="ft-text-blue-100" href="#" title="แนบไฟล์เพิ่มเติม">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M2.75 1.5a.25.25 0 00-.25.25v12.5c0 .138.112.25.25.25h10.5a.25.25 0 00.25-.25V4.664a.25.25 0 00-.073-.177l-2.914-2.914a.25.25 0 00-.177-.073H2.75zM1 1.75C1 .784 1.784 0 2.75 0h7.586c.464 0 .909.184 1.237.513l2.914 2.914c.329.328.513.773.513 1.237v9.586A1.75 1.75 0 0113.25 16H2.75A1.75 1.75 0 011 14.25V1.75zm7 1.5a.75.75 0 01.75.75v1.5h1.5a.75.75 0 010 1.5h-1.5v1.5a.75.75 0 01-1.5 0V7h-1.5a.75.75 0 010-1.5h1.5V4A.75.75 0 018 3.25zm-3 8a.75.75 0 01.75-.75h4.5a.75.75 0 010 1.5h-4.5a.75.75 0 01-.75-.75z"></path></svg>
          <span>แนบไฟล์เพิ่มเติม</span>
        </a>
      </li>
      <li class="ft-h6">
        <a class="ft-text-orange-100" href="#" title="แนบไฟล์เพิ่มเติม">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0zM8 0a8 8 0 100 16A8 8 0 008 0zm.5 4.75a.75.75 0 00-1.5 0v3.5a.75.75 0 00.471.696l2.5 1a.75.75 0 00.557-1.392L8.5 7.742V4.75z"></path></svg>
          <span>ติดตาม</span>
        </a>
      </li>
    </ul>
  </div>
</pre>
        </div>
    </div>
    <!-------------------- End Listing -------------------->
    <!-------------------- Card -------------------->
    <div id="card" class="ft-block">
        <h1 class="ft-title">Card</h1>
        <div style="padding: 20px;" class="ft-bg-grey-80">
         <?php for ($i=0; $i < 3; $i++) { ?>
            <div class="ft-card" style="margin-bottom: 15px;">
              <a class="ft-card-close">x</a>
              <div class="ft-h6" style="color: red;margin-bottom: 10px;">รายการยื่นคำขอไม่ผ่าน</div>
              <div class="ft-h5 ft-text-grey-100">เอกสารหมายเลข</div>
              <div class="ft-h3 ft-text-green-100 "><?=date("Ymd").($i+1)?></div>
              <div><a class="ft-text-blue-100 ft-h6" style="width: 85%;display: inline-block;" class="">เรียนรู้เพิ่มเติม</a><a class="ft-remark" style="width: 13%;display: inline-block;color: red;">ขอแก้ไข</a></div>
            </div>
         <?php } ?>
        </div>
        <div class="ft-code-example">
           <pre class="code" lang="html">
   <div class="ft-card" style="margin-bottom: 15px;">
     <a class="ft-card-close">x</a>
     <div class="ft-h6" style="color: red;margin-bottom: 10px;">รายการยื่นคำขอไม่ผ่าน</div>
     <div class="ft-h4 ft-text-grey-100">เอกสารหมายเลข</div>
     <div class="ft-h3 ft-text-green-100 "><?=date("Ymd")."1"?></div>
     <div><a class="ft-text-blue-100 ft-h6" style="width: 85%;display: inline-block;" class="">เรียนรู้เพิ่มเติม</a><a class="ft-remark" style="width: 13%;display: inline-block;color: red;">ขอแก้ไข</a></div>
   </div></pre>
      </div>
        <!-------------------- End Card -------------------->
    </div>
</div>

<?php include 'template/footer.php'; ?>