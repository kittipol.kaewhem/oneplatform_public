<?php include 'template/header.php';?>
<div class="ft-content">
  <div class="ft-block">
  	<div class="ft-h1 ft-title">
      One Part Layout
    </div>
    <div class="ft-container ft-1-part">
      <div class="ft-main bg-grey-80">
        <div class="ft-content no-bottom no-top">
          <div class="ft-block no-bg">
            <div class="ft-h2">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </div>
            <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
          </div>
        </div>
      </div>
    </div>
    <div class="ft-code-example">
       <pre class="code" lang="html">
  <div class="ft-container ft-1-part">
    <div class="ft-main">
      <div class="ft-content">
        <div class="ft-block">
          <div class="ft-h2">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </div>
          <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
        </div>
      </div>
    </div>
  </div></pre>
    </div>
  </div>
  <div class="ft-block">
    <div class="ft-h1 ft-title">
      Two Parts Layout
    </div>
    <div style="margin-top: 20px;" class="ft-h2">
      Right Layout
    </div>
    <div class="ft-container ft-2-part">
      <div class="ft-main bg-grey-80">
        <div class="ft-content no-bottom no-top">
          <div class="ft-block no-bg">
            <div class="ft-h2">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </div>
            <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
          </div>
        </div>
      </div>
      <div class="ft-aside-r bg-grey-80">
        <div class="ft-content no-bottom no-top">
          <div class="ft-block no-bg">
            <p class="ft-h3">SAMPLE SPACE</p>
            <p class="ft-h4">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
            <p>Eligendi laudantium, ipsa modi perferendis vitae fugiat unde sed commodi totam ullam atque laborum adipisci voluptatem quia provident possimus accusamus autem nulla?</p>
          </div>
        </div>
      </div>
    </div>
    <div class="ft-code-example">
       <pre class="code" lang="html">
  <div class="ft-container ft-2-part">
    <div class="ft-main">
      <div class="ft-content">
        <div class="ft-block">
          <div class="ft-h2">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </div>
          <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
        </div>
      </div>
    </div>
    <div class="ft-aside-r">
      <div class="ft-content">
        <div class="ft-block">
          <p class="ft-h3">SAMPLE SPACE</p>
          <p class="ft-h4">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
          <p>Eligendi laudantium, ipsa modi perferendis vitae fugiat unde sed commodi totam ullam atque laborum adipisci voluptatem quia provident possimus accusamus autem nulla?</p>
        </div>
      </div>
    </div>
  </div></pre>
    </div>
    <div  style="margin-top: 40px;" class="ft-h2">
      Left Layout
    </div>
      <div class="ft-container ft-2-part">
        <div class="ft-aside-l bg-grey-80">
          <div class="ft-content no-bottom no-top">
            <div class="ft-block no-bg">
              <p class="ft-h3">SAMPLE SPACE</p>
              <p class="ft-h4">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
              <p>Eligendi laudantium, ipsa modi perferendis vitae fugiat unde sed commodi totam ullam atque laborum adipisci voluptatem quia provident possimus accusamus autem nulla?</p>
            </div>
          </div>
        </div>
        <div class="ft-main bg-grey-80">
          <div class="ft-content no-bottom no-top">
            <div class="ft-block no-bg">
              <div class="ft-h2">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
              </div>
              <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
            </div>
          </div>
        </div>
      </div>
      <div class="ft-code-example">
         <pre class="code" lang="html">
  <div class="ft-container ft-2-part">
    <div class="ft-aside-l">
      <div class="ft-content">
        <div class="ft-block">
          <p class="ft-h3">SAMPLE SPACE</p>
          <p class="ft-h4">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
          <p>Eligendi laudantium, ipsa modi perferendis vitae fugiat unde sed commodi totam ullam atque laborum adipisci voluptatem quia provident possimus accusamus autem nulla?</p>
        </div>
      </div>
    </div>
    <div class="ft-main">
      <div class="ft-content">
        <div class="ft-block">
          <div class="ft-h2">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </div>
          <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
        </div>
      </div>
    </div>
  </div></pre>
      </div>
  </div>
  <div class="ft-block">
    <div class="ft-h1 ft-title">
      Three Parts Layout
    </div>
    <div class="ft-container ft-3-part">
      <div class="ft-aside-l bg-grey-80">
        <div class="ft-content no-bottom no-top">
          <div class="ft-block no-bg">
            <p class="ft-h5">SAMPLE SPACE</p>
            <p class="ft-remark">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
          </div>
        </div>
      </div>
      <div class="ft-main bg-grey-80">
        <div class="ft-content no-bottom no-top">
          <div class="ft-block no-bg">
            <div class="ft-h2">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </div>
            <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
          </div>
        </div>
      </div>
      <div class="ft-aside-r bg-grey-80">
        <div class="ft-content no-bottom no-top">
          <div class="ft-block no-bg">
            <p class="ft-h5">SAMPLE SPACE</p>
            <p class="ft-remark">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="ft-code-example">
      <pre class="code" lang="html">
  <div class="ft-container ft-3-part">
    <div class="ft-aside-l">
      <div class="ft-content">
        <div class="ft-block">
          <p class="ft-h5">SAMPLE SPACE</p>
          <p class="ft-remark">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
        </div>
      </div>
    </div>
    <div class="ft-main">
      <div class="ft-content">
        <div class="ft-block">
          <div class="ft-h2">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </div>
          <p> Pariatur, facilis aut cumque natus reprehenderit nesciunt. Consectetur, consequuntur quia corrupti rerum quasi nisi inventore odio, magnam nostrum quos odit tempora non?</p>
        </div>
      </div>
    </div>
    <div class="ft-aside-r">
      <div class="ft-content">
        <div class="ft-block">
          <p class="ft-h5">SAMPLE SPACE</p>
          <p class="ft-remark">Lorem ipsum, dolor sit amet, consectetur adipisicing elit.</p>
        </div>
      </div>
    </div>
  </div></pre>
    </div>
  </div>
 </div>
<?php include 'template/footer.php'; ?>