$.fn.gotoAnchor = function(opts){
	var hd;
	if($(window).width() > 767){
		hd = -146;
	}else{
		hd = -70;
	}
	if( $(this).length>0 ){
		var defaults={
			speed:'slow',
			stop:false,
			offsetTop: ($("#header").length > 0)?$("#header")[0].offsetHeight+516:0,
			anotherTop: hd,
			callBack:function(){}
		};
		$.extend(true,defaults,opts);
		if (!defaults.stop) {
			$('html,body').animate({scrollTop:$(this).offset().top+defaults.anotherTop},defaults.speed).promise().done(function(){defaults.callBack();});
	   	}else{
	   		$('html,body').stop(true,false).animate({scrollTop:$(this).offset().top+defaults.anotherTop },defaults.speed).promise().done(function(){defaults.callBack();});
	   	}
	}
}
$(document).ready(function(){
	$('pre.code').each(function(i,v){
		$(v).addClass("code"+i);
		$(v).highlight({
			zebra: false,
			source: false,
		});
	});
	if ($(document).scrollTop() > 20) {
		$('#gototop').fadeIn();
	} else {
		$('#gototop').fadeOut();
	}
	$(window).resize(function(){
		$("#ft-menu-canvas").removeClass("open");
		$("#ft-header .main-header").removeClass("canvas");
		$("html,body").attr("style","");
		$(".header-canvas .mg-item").each(function(i,v){
			$(v).removeClass("active");
		});
	});
	$(window).scroll(function(){
		if ($(this).scrollTop() > 20) {
			$('#gototop').fadeIn();
		} else {
			$('#gototop').fadeOut();
		}
	});
	$("#gototop").click(function(){
		$("#ft-header").gotoAnchor();
	});
	$('.goto').click(function(){
		$($(this).attr("data-goto")).gotoAnchor();
	});
	$("#ft-menu-canvas").click(function(){
		if(!$(this).hasClass("open")){
			$(this).addClass("open");
			$("#ft-header .main-header").addClass("canvas");
			$("html,body").attr("style","overflow:hidden");
		}else{
			$(this).removeClass("open");
			$("#ft-header .main-header").removeClass("canvas");
			$("html,body").attr("style","");
			$(".header-canvas .mg-item").each(function(i,v){
				$(v).removeClass("active");
			});
		}
	});
	$(".header-canvas .mg-item").click(function(){
		if(!$(this).hasClass("active")){
			$(this).addClass("active");
		}else{
			$(this).removeClass("active");
		}
	});
	$(".ft-tab-btn").click(function(){
		var elem = $(this);
		var	tab = elem.attr("data-tab");
		if(elem.hasClass("main")){
			elem.parents(".ft-tab-layout").removeClass("tab-play");
			$(".ft-tab-btn").each(function(i,v){
				$(v).removeClass("active");
			});
			$(".tab-item").each(function(i,v){
				$(v).removeClass("active");
			});
		}else{
			if(elem.hasClass("active")){
				elem.removeClass("active");
				elem.parents(".ft-tab-layout").removeClass("tab-play");
				$(".tab-item").each(function(i,v){
					$(v).removeClass("active");
				});
			}else{
				$(".ft-tab-btn").each(function(i,v){
					$(v).removeClass("active");
				});
				$(".tab-item").each(function(i,v){
					$(v).removeClass("active");
				});
				elem.addClass("active");
				$(".tab-item."+tab).addClass("active");
				elem.parents(".ft-tab-layout").addClass("tab-play");
			}
		}
	});
});






